<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Главная страница");
?>

    <div class="container_4">

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/01.jpg" alt="ELM Company"></a>

        </div><!--/block-->

        <div class="grid_1 block emerge">


            <!-- пример общей скидки для всех айтемов в слайдере -->
            <span class="discount"><span>−30%</span></span>

            <!-- класс radial_gradient - добавляет затенение углов у каждой ссылки внутри item. ок для 270x270 -->
            <div class = "ios default radial_gradient">

                <div class = "slider">
                    <!-- айтемы в слайдере могут быть ТОЛЬКО дивами, всё остальное - внутри -->
                    <div class="item"><a href="#"><img src="/bitrix/templates/developer_test/img/blocks/02.jpg" alt=""></a></div>
                    <div class="item"><a href="#"><img src="/bitrix/templates/developer_test/img/blocks/03.jpg" alt=""></a></div>
                    <div class="item"><a href="#"><img src="/bitrix/templates/developer_test/img/blocks/04.jpg" alt=""></a></div>
                    <div class="item"><a href="#"><img src="/bitrix/templates/developer_test/img/blocks/05.jpg" alt=""></a></div>

                </div>


                <div class="arrow prev"><span></span></div>
                <div class="arrow next"><span></span></div>

                <div class="goTo">
                    <!-- пагинация слайдера, добавляется автоматически через js -->
                </div>

            </div><!--/ios-->

        </div><!--/block-->
        <div class="grid_2 block emerge">

            <a href="#">
                <img src="/bitrix/templates/developer_test/img/blocks/13.jpg" alt="VANS. Скидки 30%">
            </a>

        </div><!--/block-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <div class="grid_2 block emerge">

            <a href="#">
                <img src="/bitrix/templates/developer_test/img/blocks/06.jpg" alt="Шлемы BURTON. Скидки 30%">
            </a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a href="#">
                <img src="/bitrix/templates/developer_test/img/blocks/ruler.png" alt="Подобрать сноуборд">
            </a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a href="#">
                <img src="/bitrix/templates/developer_test/img/blocks/14.jpg" alt="16 февраля 12:00 B-Shop day в парке Горького">
            </a>

        </div><!--/block-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


        <div class="grid_1 someinfo">
            <a href="#" class="si cart">
                <i class="ico"></i>
                <b>Большой выбор</b>
                Более 5492 товаров
            </a>
        </div><!--/someinfo-->
        <div class="grid_1 someinfo">
            <a href="#" class="si plane">
                <i class="ico"></i>
                <b>Бесплатная доставка</b>
                Более 5492 товаров
            </a>
        </div><!--/someinfo-->
        <div class="grid_1 someinfo">
            <a href="#" class="si phone">
                <i class="ico"></i>
                <b>Бесплатный возврат</b>
                Более 5492 товаров
            </a>
        </div><!--/someinfo-->
        <div class="grid_1 someinfo">
            <a href="#" class="si bubble">
                <i class="ico"></i>
                <b>Консультации</b>
                Более 5492 товаров
            </a>
        </div><!--/someinfo-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/08.jpg" alt="Ночь всех влюбленных"></a>

        </div><!--/block-->
        <div class="grid_2 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/09.jpg" alt="Скейтборды"></a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/10.jpg" alt="SAGA впервые в истории"></a>

        </div><!--/block-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <div class="grid_4 huge block emerge">

            <div class = "ios default autoplay">

                <div class = "slider">

                    <div class="item">

                        <div class="hover_fade">
                            <img src="/bitrix/templates/developer_test/img/blocks/11.jpg" alt="">


                            <div class="slide_title"><b>BURTON</b> ЗИМА 2014</div>


                            <!-- pont может быть ссылкой-->
                            <a href="#" class="point" style="left:52%;top:20%;">

                                                <span class="point_text">
                                                    <span class="whatisthat">Куртка</span>
                                                    <span class="name">Burton Poacher Jacket</span>
                                                    <span class="price">6 430 руб</span>
                                                    <span class="kindalink">Посмотреть в каталоге</span>
                                                </span>

                            </a><!--/point-->
                            <a href="#" class="point" style="left:79.1%;top:5.0%;">

                                                <span class="point_text">
                                                    <span class="whatisthat">Куртка</span>
                                                    <span class="name">Burton Poacher Jacket</span>
                                                    <span class="price">6 430 руб</span>
                                                    <span class="kindalink">Посмотреть в каталоге</span>
                                                </span>

                            </a><!--/point-->
                            <a href="#" class="point" style="left:36%;top:62%;">

                                                <span class="point_text">
                                                    <span class="whatisthat">Куртка</span>
                                                    <span class="name">Burton Poacher Jacket</span>
                                                    <span class="price">6 430 руб</span>
                                                    <span class="kindalink">Посмотреть в каталоге</span>
                                                </span>

                            </a><!--/point-->
                            <a href="#" class="point" style="left:95%;top:75%;">

                                                <span class="point_text">
                                                    <span class="whatisthat">Куртка</span>
                                                    <span class="name">Burton Poacher Jacket</span>
                                                    <span class="price">6 430 руб</span>
                                                    <span class="kindalink">Посмотреть в каталоге</span>
                                                </span>

                            </a><!--/point-->

                        </div>

                    </div>
                    <div class="item">
                        <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/12.jpg" alt=""></a>
                    </div>

                </div>

                <div class="arrow prev"><span></span></div>
                <div class="arrow next"><span></span></div>

                <div class="goTo">
                    <!-- пагинация слайдера, добавляется автоматически через js -->
                </div>

            </div><!--/ios-->

        </div><!--/block-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <div class="grid_4 allbrands">

            <div class="border">

                <ul class="simple clearfix">

                    <li>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/nike.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/obey.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/diamond.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/pig.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/neff.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/crooks.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/huf.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/vans.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/bomb.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/nixon.png" alt=""></a>
                        <a href="#"><img src="/bitrix/templates/developer_test/img/brands/primitive.png" alt=""></a>

                    </li>
                    <li>
                        <a href="#">Все бренды</a>
                    </li>

                </ul>



            </div><!--/border-->

        </div><!--/block-->


        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->


        <div class="grid_1 block">

            <div class="subscribe">

                <p>ПОЖАЛУЙСТА, ПРОВЕРЬ ПОЧТУ И&nbsp;ПОДТВЕРДИ ПОДПИСКУ</p>


                <div class="subscribe_ok"><img src="/bitrix/templates/developer_test/img/subs_ok.png" alt=""></div>



            </div>

        </div><!--/block-->
        <div class="grid_2 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/express.jpg" alt="Бесплатная экспресс-доставка"></a>

        </div><!--/block-->
        <div class="grid_1 block">

            <div class="subscribe">

                <p>ПОДПИШИСЬ И&nbsp;ПОЛУЧИ СКИДКУ&nbsp;20%</p>

                <form>
                    <input type="email" name="subscribe" id="subscribe" placeholder="твой@email">
                    <input type="submit" value="Подписаться">

                </form>

            </div>

        </div><!--/block-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/16.jpg" alt="CELTEK - перчатки, которые не подведут"></a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a class="new_stuff" href="#">
                <img src="/bitrix/templates/developer_test/img/blocks/new_stuff.jpg" alt="Новинки">
                <strong>314</strong>
                <span>товаров в марте</span>
            </a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/stores.png" alt="Наши магазины"></a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/17.jpg" alt="10 советов начинающим"></a>

        </div><!--/block-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <div class="grid_1 block">

            <div class="circe_b fs14 txt_center padd14 borderf2">ПРИСОЕДИНЯЙСЯ!</div>

            <div class="social tabs">
                <!-- в такой штучке размер соц.блоков 236x149px -->
                <ul class="soc_tabs h_tabs simple clearfix">
                    <li class="fb" data-target=".fb_tab"><i>fb</i></li>
                    <li class="vk" data-target=".vk_tab"><i>vk</i></li>
                    <li class="ok" data-target=".ok_tab"><i>tw</i></li>
                    <li class="in" data-target=".in_tab"><i>in</i></li>
                </ul>

                <div class="tabs_wrapper">

                    <div class="fb_tab">



                        fb

                    </div><!--/fb_tab-->

                    <div class="vk_tab">

                        vk

                    </div><!--/vk_tab-->

                    <div class="ok_tab">


                        ok

                    </div><!--/tw_tab-->
                    <div class="in_tab">


                        in

                    </div><!--/in_tab-->

                </div><!--/tabs_wrapper-->


            </div><!--/social.tabs-->

        </div><!--/block-->

        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/18.jpg" alt="Парафиним доску"></a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a class="social_block" href="#"><img src="/bitrix/templates/developer_test/img/blocks/soc_vk.png" alt=""></a>
            <a class="social_block" href="#"><img src="/bitrix/templates/developer_test/img/blocks/soc_fb.png" alt=""></a>
            <a class="social_block" href="#"><img src="/bitrix/templates/developer_test/img/blocks/soc_ok.png" alt=""></a>
            <a class="social_block" href="#"><img src="/bitrix/templates/developer_test/img/blocks/soc_inst.png" alt=""></a>

        </div><!--/block-->
        <div class="grid_1 block emerge">

            <a href="#"><img src="/bitrix/templates/developer_test/img/blocks/18.jpg" alt="Парафиним доску"></a>

        </div><!--/block-->

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

    </div><!--/container_4-->


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>