<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
global $USER;
?>
<!DOCTYPE html>
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title><?$APPLICATION->ShowTitle();?></title>
    <?$APPLICATION->ShowMeta("description");?>
    <?$APPLICATION->ShowMeta("keywords");?>
    <?$APPLICATION->ShowMeta("robots");?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1180">

    <link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon" href="/favicon.png" type="image/png" />
    <link rel="apple-touch-icon-precomposed" href="/apple-touch-icon.png" />

    <!--[if lt IE 9]><script src="<?=SITE_TEMPLATE_PATH?>/js/html5.js"></script><![endif]-->

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/normalize.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/grid.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">

    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.11.1.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr_custom.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.easing-1.3.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mousewheel.min.js"></script>


    <script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>

    <!--[if gt IE 8]><!-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/emerge.js"></script>
    <!--<![endif]-->

    <?$APPLICATION->ShowCSS(true);?>
    <?$APPLICATION->ShowHeadStrings();?>
    <?$APPLICATION->ShowHeadScripts();?>
    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</head>
<body>
<?$APPLICATION->ShowPanel()?>


<div class="social out fixed">
    <div class="s_wrap">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "social_right",
                "AREA_FILE_RECURSIVE" => "Y",
            ),
            false
        );?>
    </div><!--/s_wrap-->
</div><!--/social_out-->

<div class="main_wrapper">

<div class="nav_wrapper fixed">
<div class="shadow"><div></div></div><!--/shadow-->
<div class="nav_content clearfix">

<div class="rightside clearfix">
    <?if(!$USER->IsAuthorized()){?>
        <div class="toplvl user_login onclick">
            <span class="trigger"><i class="ico"></i><a href="/personal/">Вход</a></span>
        </div><!--/toplvl-->
    <?}else{?>
        <div class="toplvl user_login hassub">
            <a href="/personal/" class="trigger"><i class="ico"></i><?=$USER->GetFirstName()." ".$USER->GetLastName()?></a>
            <div class="sub">
                <div class="sub_content">
                    <ul class="submenu">
                        <li><a class="fs13 gray_link" href="?logout=yes">Выйти</a></li>
                    </ul>
                </div><!--sub_content-->
            </div><!--/sub-->
        </div><!--/toplvl-->
    <?}?>

</div><!--/fl_right-->

<nav class="main">
<?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "top_custom",
    Array(
        "ROOT_MENU_TYPE" => "top_custom",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array("", ""),
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    )
)?>
<?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "top",
    Array(
        "ROOT_MENU_TYPE" => "top",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array("", ""),
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    )
)?>
</nav>
</div><!--/nav_content-->
</div><!--/nav_wrapper-->


<div class="content_wrapper" style="<?$APPLICATION->ShowProperty("PAGE_STYLE")?>">
<div class="main_content">
<header class="main">



<div class="top_line">


    <ul class="simple logos clearfix">
        <li>
            <a href="/" class="logo">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_RECURSIVE" => "Y",
                "PATH" => SITE_TEMPLATE_PATH."/inc_areas/logo.php",
            ),
            false
        );?>
            </a>
        </li>
        <li>
        </li>
        <li>
        </li>
    </ul><!--logos-->

</div><!--/top_line-->
<div class="bottom_line clearfix">


</div><!--/bottom_line-->
</header>
<article class="main">
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
            "START_FROM" => "0",
            "PATH" => "",
            "SITE_ID" => "s1"
        )
    );?>