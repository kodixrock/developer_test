<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
</article>
<aside class="main">


</aside>

</div><!--/main_content-->
</div><!--/content_wrapper-->


<footer class="main">

    <div class="footer_content">

        <div class="footer clearfix">

            <div class="copy fl_left">© develeper_test, <?=date('Y')?></div>


            <div class="logos fl_left clearfix">
                <ul class="simple clearfix">
                    <li><span class="visa"></span></li>
                    <li><span class="mastercard"></span></li>
                    <li><span class="maestro"></span></li>
                </ul>
                <ul class="simple clearfix">
                    <li><span class="box"></span></li>
                    <li><span class="pony"></span></li>
                    <li><span class="pr"></span></li>
                    <li><span class="delo"></span></li>
                    <li><span class="qiwi"></span></li>
                    <li><span class="ems"></span></li>
                    <li><span class="yes"></span></li>
                </ul>

            </div><!--/-->

        </div><!--/footer-->
    </div><!--/footer_content-->

</footer>


</div><!--/main_wrapper-->
</body>
</html>
