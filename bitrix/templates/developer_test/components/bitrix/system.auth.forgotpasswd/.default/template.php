<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
ShowMessage($arParams["~AUTH_RESULT"]);
?>
<div class="message_quasicentral ml380">

    <h2>Восстановление пароля</h2>
    <div class="std_form semilong_label margintop2x">

        <form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">

            <div class="form_row ">
                <label for="email">E-mail</label>
                <input type="text" id="email" name="USER_EMAIL" value="">
            </div><!--/form_row-->

            <div class="form_row">
                <!-- пустой label создает необходимый отступ -->
                <label>&nbsp;</label>
                <input name="send_account_info" type="submit" value="Восстановить">
            </div><!--/form_row-->

            <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                <noindex>
                    <p>
                        <a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow">Авторизоваться</a>
                    </p>
                </noindex>
                <noindex>
                    <p>
                        <a href="/registration/" rel="nofollow">Регистрация</a><br />
                        <?=GetMessage("AUTH_FIRST_ONE")?>
                    </p>
                </noindex>
            <?endif?>

        </form>
    </div>


</div><!--/message_central-->
<script type="text/javascript">
    document.bform.USER_EMAIL.focus();
</script>