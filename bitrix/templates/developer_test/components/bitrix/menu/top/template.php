<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<ul class="simple clearfix">
    <?if (!empty($arResult)){?>
            <?foreach ($arResult as $key => $value): ?>
                <?if($value['DEPTH_LEVEL'] == 1):?>
                    <li class="toplvl<?if($value['SELECTED']){?> current<?}?>"><a href="<?=$value['LINK']?>"><?=$value['TEXT']?></a></li>
                <?endif?>
            <?endforeach ?>
    <?}?>
</ul>