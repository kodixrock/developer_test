<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?//printr($arResult);?>
<?if (!empty($arResult)){
$head_elements=array();
$foot_elements=array();
$middle_elements=array();
foreach($arResult as $k=>$item){
    $column=intval($item["PARAMS"]["COLUMN"])?:2;
    if($item["PARAMS"]["IN_TOP"]=="Y")
        $head_elements[$column][]=$item;
    elseif($item["PARAMS"]["IN_FOOT"]=="Y")
        $foot_elements[$column][]=$item;
    else{
        $middle_elements[$column][]=$item;
    }
        //continue;
    //unset($arResult[$k]);
}
/*$menu=new CMenu("main");
$menu->Init($APPLICATION->GetCurDir(), true);
$middle_elements[1]=$menu->arMenu;*/
?>
<ul class="simple clearfix custom_menu">
    <li class="toplvl hassub first_menu">
        <span class="trigger list_icon"><span></span><span></span><span></span></span>
        <div class="sub">
            <div class="sub_content">
                <div class="columns clearfix">
                    <?if(!empty($head_elements[1]) || !empty($middle_elements[1]) || !empty($foot_elements[1])){?>
                    <div class="column">
                        <?if(!empty($head_elements[1])){?>
                            <ul class="submenu">
                                <?foreach($head_elements[1] as $item){?>
                                    <li><a href="<?=$item["LINK"]?>"><?=$item["TEXT"]?></a></li>
                                <?}?>
                            </ul>
                        <?}?>
                        <?if(!empty($middle_elements[1])){?>
                            <ul class="submenu">
                                <?foreach($middle_elements[1] as $item){
                                    if(isset($item[3]["DEPTH_LEVEL"]) && $item[3]["DEPTH_LEVEL"]!=1)
                                        continue;
                                 ?>
                                    <li><a href="<?=($item[1]?:$item["LINK"])?>"><?=($item[0]?:$item["TEXT"])?></a></li>
                                <?}?>
                            </ul>
                        <?}?>
                        <?if(!empty($foot_elements[1])){?>
                            <ul class="submenu">
                                <?foreach($foot_elements[1] as $item){?>
                                    <li><a href="<?=$item["LINK"]?>"><?=$item["TEXT"]?></a></li>
                                <?}?>
                            </ul>
                        <?}?>
                    </div><!--/column-->
                    <?}?>
                    <div class="column">
                        <?if(!empty($head_elements[2])){?>
                            <ul class="submenu">
                                <?foreach($head_elements[2] as $item){?>
                                    <li><a href="<?=$item["LINK"]?>"><?=$item["TEXT"]?></a></li>
                                <?}?>
                            </ul>
                        <?}?>
                        <?if(!empty($middle_elements[2])){?>
                            <ul class="submenu">
                                <?foreach($middle_elements[2] as $item){
                                    if(isset($item[3]["DEPTH_LEVEL"]) && $item[3]["DEPTH_LEVEL"]!=1)
                                        continue;
                                    ?>
                                    <li><a href="<?=($item[1]?:$item["LINK"])?>"><?=($item[0]?:$item["TEXT"])?></a></li>
                                <?}?>
                            </ul>
                        <?}?>
                        <?if(!empty($foot_elements[2])){?>
                            <ul class="submenu">
                                <?foreach($foot_elements[2] as $item){?>
                                    <li><a href="<?=$item["LINK"]?>"><?=$item["TEXT"]?></a></li>
                                <?}?>
                            </ul>
                        <?}?>
                    </div><!--/column-->
                </div><!--/columns-->
            </div><!--sub_content-->
        </div><!--/sub-->
    </li>
</ul>
<?}?>