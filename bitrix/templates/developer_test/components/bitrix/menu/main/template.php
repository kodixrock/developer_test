<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?if (!empty($arResult)){?>
    <?$items_per_column=KDXSettings::getSetting('COUNT_ITEMS_IN_MENU_COLUMN');?>
    <?$arReplace=array();?>
    <?$old_depth=1;?>
    <?$old_key=null;?>
    <?$last_MEGA_REPLACE_parent_code=null;?>
    <?ob_start();?>
<ul class="simple sub_nav clearfix">
    <?foreach ($arResult as $key => $value): ?>
        <?$current_depth=$value['PARAMS']['DEPTH_LEVEL']?:($value['DEPTH_LEVEL']?:1)?>
        <?$IS_PARENT = $value['PARAMS']['IS_PARENT']?:($value['IS_PARENT']?:'N');?>
        <?if($current_depth == 1){?>
            <?if($old_depth>$current_depth){//сначала закрываем предыдущий?>
            <?if(!is_null($old_key)){?>
            <? $prev_depth_level = ($arResult[$old_key]['PARAMS']['DEPTH_LEVEL'] ? : ($arResult[$old_key]['DEPTH_LEVEL'] ? : 1));
            //echo "<b>$prev_depth_level</b>";
            if($prev_depth_level ==2){?>
                </ul>
            <?}elseif($prev_depth_level==3){?>
                </ul>
                </div>
                </div><!--/tabs_wrapper-->
                </div>
                <?$arReplace['<div class="MEGA_REPLACE_'.$last_MEGA_REPLACE_parent_code.'"></div>'].='</ul>';?>
            <?}?>
                </div><!--sub_content-->
                </div><!--/sub-->
                </li>
            <?}?>
            <?}?>
            <?if($IS_PARENT !='Y'){?>
                <li class="<?if($value['PARAMS']['CLASS']){echo $value['PARAMS']['CLASS'];}?><?if($value['SELECTED']){?> current<?}?>"><a href="<?=$value['LINK']?>"><?=$value['TEXT']?></a></li>
            <?}else{?>
                <li class="hassub <?if($value['PARAMS']['CLASS']){echo $value['PARAMS']['CLASS'];}?><?if($value['SELECTED']){?> current<?}?>">
                    <a class="trigger" href="<?=$value['LINK']?>"><?=$value['TEXT']?></a>
                    <div class="sub">
                        <div class="sub_content">

            <?}?>
        <?}elseif($current_depth == 2){?>
            <?if($IS_PARENT !='Y'){?>
                <?if($old_depth<$current_depth){?>
                            <ul class="submenu make_cols_<?=(($arResult[$old_key]['PARAMS']['COUNT_ITEMS']/$items_per_column>3)?4:ceil($arResult[$old_key]['PARAMS']['COUNT_ITEMS']/$items_per_column))?> clearfix">
                <?}?>
                                <li class="<?if($value['PARAMS']['CLASS']){echo $value['PARAMS']['CLASS'];}?><?if($value['SELECTED']){?> current<?}?>"><a href="<?=$value['LINK']?>"><?=$value['TEXT']?></a></li>

            <?}else{?>
                <?if($old_depth>$current_depth){//сначала закрываем предыдущий?>
                                </ul>
                            </div>
                <?}?>
                <?if($old_depth<$current_depth){?>

                    <div class="in_menu tabs">
                        <div class="MEGA_REPLACE_<?=$value['PARAMS']['PARENT_CODE']?>"></div>
                        <div class="tabs_wrapper">
                    <?//printr($value);?>
                       <?$arReplace['<div class="MEGA_REPLACE_'.$value['PARAMS']['PARENT_CODE'].'"></div>']='
                       <ul class="h_tabs simple clearfix">';
                        ?>
                <?}?>
                            <div class="<?=$value['PARAMS']['CODE']?>">
                            <?$arReplace['<div class="MEGA_REPLACE_'.$value['PARAMS']['PARENT_CODE'].'"></div>'].='
                            <li data-target=".'.$value['PARAMS']['CODE'].'"><a href="'.$value['LINK'].'">'.$value['TEXT'].'</a></li>';
                            $last_MEGA_REPLACE_parent_code=$value['PARAMS']['PARENT_CODE'];
                            ?>
            <?}?>
        <?}else{?>
            <?if($old_depth<$current_depth){?>
                <ul class="submenu make_cols_<?=(($arResult[$old_key]['PARAMS']['COUNT_ITEMS']/$items_per_column>3)?4:ceil($arResult[$old_key]['PARAMS']['COUNT_ITEMS']/$items_per_column))?> clearfix" >
            <?}?>
                    <li class="<?if($value['PARAMS']['CLASS']){echo $value['PARAMS']['CLASS'];}?><?if($value['SELECTED']){?> current<?}?>"><a href="<?=$value['LINK']?>"><?=$value['TEXT']?></a></li>
        <?}?>

        <?$old_depth=$current_depth;?>
        <?$old_key=$key;?>
    <?endforeach ?>

    <?if(!is_null($old_key)){?>
        <? $prev_depth_level = ($arResult[$old_key]['PARAMS']['DEPTH_LEVEL'] ? : ($arResult[$old_key]['DEPTH_LEVEL'] ? : 1));
        //echo "<b>$prev_depth_level</b>";
        if($prev_depth_level ==2){?>
            </ul>

            </div><!--sub_content-->
            </div><!--/sub-->
            </li>
        <?}elseif($prev_depth_level==3){?>
            </ul>
            </div>
            </div><!--/tabs_wrapper-->
            </div>

            </div><!--sub_content-->
            </div><!--/sub-->
            </li>
            <?$arReplace['<div class="MEGA_REPLACE_'.$last_MEGA_REPLACE_parent_code.'"></div>'].='</ul>';?>
        <?}?>
    <?}?>
</ul>
    <?$srt_result = ob_get_contents();
    ob_end_clean();
    $srt_result=str_replace(array_keys($arReplace),array_values($arReplace),$srt_result);
    echo $srt_result;
    //printr($arResult);
    ?>
<?}?>