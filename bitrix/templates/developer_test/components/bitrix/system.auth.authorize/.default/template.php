<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
    $params=explode("?", $arResult["AUTH_FORGOT_PASSWORD_URL"]);
    $arResult["AUTH_FORGOT_PASSWORD_URL"]="/auth/?".$params[1];
}
?>
<div class="message_quasicentral ml380">
<?if($arResult["AUTH_SERVICES"]):?>
	<div class="bx-auth-title"><?echo GetMessage("AUTH_TITLE")?></div>
<?endif?>
        <h2>ВХОД НА САЙТ</h2>
    <div class="std_form semilong_label margintop2x">
	    <form name="form_auth" class="authorize_form" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <?if (strlen($arResult["BACKURL"]) > 0):?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif?>
            <?foreach ($arResult["POST"] as $key => $value):?>
            <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?endforeach?>

            <div class="form_row ">
                <label for="USER_LOGIN">E-mail</label>
                <input type="text" id="USER_LOGIN" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>">
            </div><!--/form_row-->

            <div class="form_row ">
                <label for="USER_PASSWORD"><?=GetMessage("AUTH_PASSWORD")?></label>
                <input type="password" id="USER_PASSWORD" name="USER_PASSWORD" value="">
            </div><!--/form_row-->

            <?if($arResult["SECURE_AUTH"]):?>
                <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                        <div class="bx-auth-secure-icon"></div>
                    </span>
                <noscript>
                    <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
                        <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                    </span>
                </noscript>
                <script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                </script>
            <?endif?>

            <?if($arResult["CAPTCHA_CODE"]):?>
                <div class="form_row ">
                    <label for="captcha_word"><?=GetMessage("AUTH_CAPTCHA_PROMT")?>:</label>
                    <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                </div><!--/form_row-->
                <div class="form_row ">
                    <label for="captcha_word">&nbsp;</label>
                    <input class="bx-auth-input" type="text" id="captcha_word" name="captcha_word" maxlength="50" value="" size="15" />
                </div><!--/form_row-->
            <?endif;?>
            <div class="form_row margintop">
            <?if ($arResult["STORE_PASSWORD"] == "Y"):?>
               <label class="checkbox special" for="USER_REMEMBER"><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />Запомнить меня</label>
            <?endif?>
            </div>

            <div class="form_row ">
                <label>&nbsp;</label>
                <input type="submit" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />
            </div><!--/form_row-->

            <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
            <noindex>
                <p>
                    <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
                </p>
            </noindex>
            <?endif?>

            <?if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
            <noindex>
                <p>
                    <a href="/registration/" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a><br />
                </p>
            </noindex>
            <?endif?>

	    </form>

    </div>
</div>
<script type="text/javascript">
<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>
</script>

<?if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
	array(
		"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
		"CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
		"AUTH_URL" => $arResult["AUTH_URL"],
		"POST" => $arResult["POST"],
		"SHOW_TITLES" => $arResult["FOR_INTRANET"]?'N':'Y',
		"FOR_SPLIT" => $arResult["FOR_INTRANET"]?'Y':'N',
		"AUTH_LINE" => $arResult["FOR_INTRANET"]?'N':'Y',
	),
	$component,
	array("HIDE_ICONS"=>"Y")
);
?>
<?endif?>
