<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<? foreach ($arResult as $item) {?>
    <div style="border-top: 1px solid #888;">
        <h2><?=$item['NAME']?></h2>
        <span>Части:</span>
        <ul><?//todo: для изначальной базы для двух элементов должны выводиться по 3 части?>
            <?foreach ($item['PROPERTY_PARTS_VALUE'] as $part) {?>
                <li><?=$part?></li>
            <?}?>
        </ul>
    </div>
<?}?>