<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$USER->IsAuthorized()){
    echo 'Информация доступна всем авторизованным пользователям';
}
else {

    if (!CModule::IncludeModule('iblock')) die('iblock module');
    $arOrder = array(
    );
    $arFilter = array(
        'IBLOCK_ID' => 1,
    );
    $arSelect = array(
        'NAME',
        'DETAIL_PAGE_URL',
        'PREVIEW_PICTURE',
        'PROPERTY_PARTS'
    );
    //todo сделать постраничку
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    while ($fields = $res->Fetch()) {
        $arResult[]=$fields;
    }
}
$this->IncludeComponentTemplate();
?>